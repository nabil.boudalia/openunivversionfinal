import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { MainComponent } from './main/main.component';
import { LeftsideComponent } from './main/leftside/leftside.component';
import { RightsideComponent } from './main/rightside/rightside.component';
import { PagehtmlfooterComponent } from './pagehtmlfooter/pagehtmlfooter.component';
import { GlobalContainerComponent } from './global-container/global-container.component';
import { ConfidentialityComponent } from './confidentiality/confidentiality.component';
import { PracticalinformationComponent } from './practicalinformation/practicalinformation.component';
import { TermsofuseComponent } from './termsofuse/termsofuse.component';
import { ContactComponent } from './contact/contact.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    MainComponent,
    LeftsideComponent,
    RightsideComponent,
    PagehtmlfooterComponent,
    GlobalContainerComponent,
    ConfidentialityComponent,
    PracticalinformationComponent,
    TermsofuseComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
