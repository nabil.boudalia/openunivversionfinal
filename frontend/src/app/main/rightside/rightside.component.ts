import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import axios from 'axios';

@Component({
  selector: 'app-rightside',
  templateUrl: './rightside.component.html',
  styleUrls: ['./rightside.component.css']
})
export class RightsideComponent implements OnInit {

  addUser = new FormGroup({
    firstname: new FormControl(''),
    lastname: new FormControl(''),
    email: new FormControl(''),
    conf_email: new FormControl(''),
    password: new FormControl(''),
    conf_password: new FormControl(''),
    day: new FormControl(''),
    month: new FormControl(''),
    year: new FormControl(''),
    gender: new FormControl(''),
  });

  constructor() { }

  ngOnInit() {
  }

  async onSubmit() {

    try {

      const res = await axios({
        method: 'post',
        url: 'http://localhost:8000/user/create',
        data: {
          firstname: this.addUser.value.firstname,
          lastname: this.addUser.value.lastname,
          email: this.addUser.value.email,
          password: this.addUser.value.password,
          birthday:`${this.addUser.value.year}-${this.addUser.value.month}-${this.addUser.value.day}`,
          gender: this.addUser.value.gender,
        },
        headers: {
          'Content-Type': 'text/plain;charset=utf-8',
        }
      });
      
      console.log(res);

    } catch(err) {

      console.log(err);
    }
  }

}
