import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagehtmlfooterComponent } from './pagehtmlfooter/pagehtmlfooter.component';
import { MainComponent } from './main/main.component';
import { ConfidentialityComponent } from './confidentiality/confidentiality.component';
import { PracticalinformationComponent } from './practicalinformation/practicalinformation.component';
import { TermsofuseComponent } from './termsofuse/termsofuse.component';
import { ContactComponent } from './contact/contact.component';


const routes: Routes = [
  {path:'', component: MainComponent },
  {path:'about', component:PagehtmlfooterComponent},
  {path:'confidentiality', component : ConfidentialityComponent},
  {path:'practicalinformation', component: PracticalinformationComponent},
  {path :'termsofuse', component : TermsofuseComponent},
  {path : 'contact', component : ContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
