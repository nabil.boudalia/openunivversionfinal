# **Projet final - Simplon**

Dans le cadre de ma formation à simplon, nous devions réaliser un projet qui systhètise nous compétences aquise.



## Le projet 

Openuniv est une application web qui permet le partage de ressources universitaires. 

Le but étant que des étudiants ou des professeurs puissent partager leurs ressources à d'autre étudiants. Le site sera accassible à tous indépendament de leur statut personnel. 

## La philosophie 

Le concepte qui à motivé ce projet est celui de l'économie de la connaissance. 

Développé par l'économiste Autrichien par Fritz Machlup en 1962 dans the production of distribution of knowledge, et dans sa thèse de 1977. Ce dernier montre que 45% de la population actives aux Etats-unis manipulent de l'information au quotidien.

Ces travaux sont repris en France en 1978, dans l'Informatisation de la société.

On estime que 34% du PIB des Etats-unis en 1980 est généré par l'industrie de la connaissance. selon Rubin et Taylor. 

Un tel projet permetterai de reclasser les acquises universitaire dans le domaine du bien communs et public non rivale.

En effet le caractère public peut être retenue puisque les universitaires bénéfice des ressources publics. 

Use case 

Users case

extends/include 



## UserStory 

* UserStory n°1

**EN TANT QU** 'étudiant

**JE VEUX** accéder aux ressources universitaires des années précédentes de mon coursus

**AFIN** de pourvoir récupérer des ressources manquant ou de mieux préparer mes examens

**ETANT DONNE** que ces ressources sont déjà disponible par d'autre ou dispersé sur internet

* UserStory n°2

**EN TANT QUE** professeur

**JE VEUX** diffuser mon cours à un maximun d'étudiant ou de personne 

**AFIN** de partager mes ressources aux plus grands nombres.

* UserStory n°3

**EN TANT** qu'individus extérieurs 

**JE VEUX** accéder aux contenues qu'une université enseigne

**AFIN** de pourvoir soit apprendre une nouvelle connaissance ou d'actualiser un acquis passé.

## Les maquettes

Version Bureau : 

![](/img-gitlab/OpenUniv_DesktopV1.png)



J'ai également imaginé un design plus épuré, qui correspond aux tendances actuelles.

![](/img-gitlab/OpenUnivDesktopV2.png)


J'ai soumis ce choix au vote

Version mobile : 

![](/img-gitlab/OpenUnivMobile.png)

Exemple de page de connection 

![](/img-gitlab/FirstPage.png)

![](/img-gitlab/ProfilPage.png)



