<?php
namespace App\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\User;
use App\Form\UserType;
use App\Encoder\Configuration;

$config = $container->get(Configuration::class);
/**
 * Movie controller.
 * @Route("/user", name="_")
 */
class UserController extends FOSRestController {
  /**
   * Create User.
   * @Rest\Post("/create")
   *
   * @return Response
   */
  public function postUserAction(Request $request)
  {
    $user = new User();
    $form=$this->createForm(UserType::class,$user);
    $data = json_decode($request->getContent(), true);
    $form->submit($data);
    if($form->isSubmitted() && $form->isValid()){
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        return $this->handleView($this->view(['status' => 'ok'], Response::HTTP_CREATED));
    }
    return $this->handleView($this->view($form->getErrors()));
  }

  /**
   * Login User.
   * @Rest\Post("/login")
   *
   * @return Response
   */
  public function loginUserAction(Request $request)
  {
    $login = $request->request->get('login');
    $password = $request->request->get('password');
    $user = $this->getDoctrine()
        ->getRepository(User::class)
        ->findOneBy([
          'email' => $login,
    ]);
    
    if($password == $user->getPassword()) {

      $tokenContent = array(
          "id" => $user->getId(),
      );

      return $this->handleView($this->view(['token' => $tokenContent], Response::HTTP_CREATED));
    }

    return $this->handleView($this->view(['error' => 'bad credentials'], Response::HTTP_UNAUTHORIZED));
  }
}